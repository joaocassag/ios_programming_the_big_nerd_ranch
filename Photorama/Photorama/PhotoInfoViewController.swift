//
//  PhotoInfoViewController.swift
//  Photorama
//
//  Created by Joao Cassamano on 12/6/20.
//  Copyright © 2020 Joao Cassamano. All rights reserved.
//

import UIKit

class PhotoInfoViewController:  UIViewController {
    @IBOutlet var imageView: UIImageView!
    
    var photo: Photo {
        didSet{
            navigationItem.title = photo.title
        }
    }
    var store: PhotoStore!
    
    //TODO: This was added becasue it wouldn't build. Check dependency injection ...
    required init?(coder aDecoder: NSCoder) {
        self.photo = Photo()
        super.init(coder: aDecoder)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.accessibilityLabel = photo.title
        
        store.fetchImage(for: photo){ (result) -> Void in
            switch result {
            case let .success(image):
                self.imageView.image = image
            case let .failure(error):
                print("Error fetching image for photo: \(self.photo)")
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showTags"?:
            let navController = segue.destination as! UINavigationController
            let tagController = navController.topViewController as! TagsViewController
            
            tagController.store = store
            tagController.photo = photo
        default:
            preconditionFailure("Unexpected segue identifier.")
        }
    }
}
